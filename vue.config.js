var path = require('path');
function configSVGIcon(config) {
    // Exclude SVG sprite directory from default svg rule
    config.module
        .rule('svg')
        .exclude.add(path.resolve(__dirname, './src/svg-icons'))
        .end();

    // Options used by svgo-loader to optimize SVG files
    // https://github.com/svg/svgo#what-it-can-do
    const options = {
        "plugins": [
            { "cleanupAttrs": false },
            { "cleanupEnableBackground": false },
            { "cleanupIDs": false },
            { "cleanupListOfValues": false },
            { "cleanupNumericValues": false },
            { "collapseGroups": false },
            { "convertColors": false },
            { "convertPathData": false },
            { "convertShapeToPath": false },
            { "convertStyleToAttrs": false },
            { "convertTransform": false },
            { "mergePaths": false },
            { "removeComments": false },
            { "removeDesc": false },
            { "removeDimensions": false },
            { "removeDoctype": false },
            { "removeEditorsNSData": false },
            { "removeEmptyAttrs": false },
            { "removeEmptyContainers": false },
            { "removeEmptyText": false },
            { "removeHiddenElems": false },
            { "removeMetadata": false },
            { "removeNonInheritableGroupAttrs": false },
            { "removeRasterImages": false },
            { "removeTitle": false },
            { "removeUnknownsAndDefaults": false },
            { "removeUselessDefs": false },
            { "removeUnusedNS": false },
            { "removeUselessStrokeAndFill": false },
            // {
            //     "removeAttrs": { "attrs": "fill"} //移除fill属性
            // },
            { "removeXMLProcInst": false },
            { "removeStyleElement": false },
            { "removeUnknownsAndDefaults": false},
            { "sortAttrs": false }
        ]
    };

    // Include only SVG sprite directory for new svg-icon rule
    // Use svg-sprite-loader to build SVG sprite
    // Use svgo-loader to optimize SVG files
    config.module
        .rule('svg-icon')
        .test(/\.svg$/)
        .include.add(path.resolve(__dirname, './src/svg-icons'))
        .end()
        .use('svg-sprite-loader')
        .loader('svg-sprite-loader')
        .options({
            symbolId: 'icon-[name]',
        })
        .end()
        .use('svgo-loader')
        .loader('svgo-loader')
        .options(options)
        .end();
}

module.exports = {
    chainWebpack: config => {
        configSVGIcon(config);
    },
};