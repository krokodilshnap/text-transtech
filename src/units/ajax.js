import axios from 'axios';

function ajaxGet() {
    var call;
    return function(url) {
        if (call) {
            call.cancel();
        }
        call = axios.CancelToken.source();
        return axios.get(url, { cancelToken: call.token });
    }
}

function ajaxPost() {
    var call;
    return function(url, data) {
        if (call) {
            call.cancel();
        }
        call = axios.CancelToken.source();
        return axios.post(url, data, { cancelToken: call.token });
    }
}

let AJAX_POST = ajaxPost(),
    AJAX_GET = ajaxGet();

export {AJAX_GET, AJAX_POST}
